# NOTE: This repository is now an archive and future development will be on [GitHub](https://github.com/cybermoloch/PSAppDeployToolkit-Apps)

# PSADTPlus-Apps

Collection of JSON files and other assets to deploy applications using PSADT-Plus

## Trademark Notice

All logos and other trademarks are property of their respective owners. They are provided here only to assist in application deployment. Please open an issue for concerns relating to logos, icons, application name usage, etc.
